//
//  KPENamesScores.h
//  projectEuler
//
//  Created by Peter Kimball on 2/17/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPEPresenter.h"

@interface KPENamesScores : NSObject <KPEProblem>

@end
