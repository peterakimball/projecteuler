//
//  KICStackBasedQueue.m
//  projectEuler
//
//  Created by Peter Kimball on 8/9/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KICStackBasedQueue.h"

@interface KICStack : NSObject

- (id)pop;
- (void)push:(id)object;
- (NSUInteger)count;

@end

@implementation KICStack {
    NSMutableArray *_backingStore;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _backingStore = [NSMutableArray new];
    }
    return self;
}

- (id)pop {
    id retVal = _backingStore.lastObject;
    [_backingStore removeLastObject];
    return retVal;
}

- (void)push:(id)object {
    [_backingStore addObject:object];
}

- (NSUInteger)count {
    return _backingStore.count;
}

@end

@implementation KICStackBasedQueue {
    KICStack *_inputStack;
    KICStack *_outputStack;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _inputStack = [KICStack new];
        _outputStack = [KICStack new];
    }
    return self;
}

- (void)enqueue:(id)object {
    [_inputStack push:object];
}

- (id)dequeue {
    if (_outputStack.count == 0) {
        while (_inputStack.count > 0) {
            [_outputStack push:[_inputStack pop]];
        }
    }
    return [_outputStack pop];
}

- (NSUInteger)count {
    return _inputStack.count + _outputStack.count;
}

@end
