//
//  NSArray+MathTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/14/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "NSArray+Math.h"

@interface NSArray_MathTests : XCTestCase

@end

@implementation NSArray_MathTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAdditionNotEmpty {
    NSArray *left = @[ @0 ];
    NSArray *right = @[ @0 ];
    
    NSArray *sum = [left arrayByAddingDigitsFromArray:right];
    
    XCTAssertEqual([sum count], 1, "Expected result of length 1");
}

- (void)testAdditionOfZero {
    NSArray *left = @[ @0 ];
    NSArray *right = @[ @0 ];
    
    NSArray *sum = [left arrayByAddingDigitsFromArray:right];
    
    XCTAssertEqual([sum[0] unsignedIntegerValue], 0, "Expected 1st digit equal to 0");
}

- (void)testAdditionWithoutCarry {
    NSArray *left = @[ @1 ];
    NSArray *right = @[ @2 ];
    
    NSArray *sum = [left arrayByAddingDigitsFromArray:right];
    
    XCTAssertEqual([sum[0] unsignedIntegerValue], 3, "Expected 1st digit equal to 3");
}

- (void)testAdditionWithCarry {
    NSArray *left = @[ @1 ];
    NSArray *right = @[ @9 ];
    
    NSArray *sum = [left arrayByAddingDigitsFromArray:right];
    
    XCTAssertEqual([sum[0] unsignedIntegerValue], 0, "Expected 2nd digit equal to 0");
    XCTAssertEqual([sum[1] unsignedIntegerValue], 1, "Expected 1st digit equal to 1");
}

- (void)testAdditionWithLongDistanceCarry {
    NSArray *left = @[ @9, @9 ];
    NSArray *right = @[ @9, @9, @9, @9 ];
    
    NSArray *sum = [left arrayByAddingDigitsFromArray:right];
    NSArray *expected = @[ @8, @9, @0, @0, @1 ];

    for (int i = 0; i < expected.count; i++) {
        XCTAssertEqual([sum[i] unsignedIntegerValue], [expected[i] unsignedIntegerValue], @"result at index %d not %u", i, [expected[i] unsignedIntValue]);
    }
}

- (void)testIntegerString {
    NSArray *lsdAtIndexZero = @[ @0, @0, @1 ];
    NSLog(@"%@", [lsdAtIndexZero integerString]);
    XCTAssertEqualObjects([lsdAtIndexZero integerString], @"100", @"expected 100");
}

@end
