//
//  KICFiveSidedDieFromSevenSidedDieTests.m
//  projectEuler
//
//  Created by Peter Kimball on 8/10/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KICDice.h"

@interface KICFiveSidedDieFromSevenSidedDieTests : XCTestCase

@property (nonatomic) NSMutableArray *hitCounts;

@end

@implementation KICFiveSidedDieFromSevenSidedDieTests

- (void)testRandomFunctionCalledFiveTimes {
    __block NSUInteger randomWasCalled = 0;
    
    NSUInteger (^mockRandom)(u_int32_t) = ^NSUInteger(u_int32_t randMax) {
        randomWasCalled++;
        return 0;
    };
    KICDice *sut = [[KICDice alloc] initWithRandomGenerator:mockRandom];
    [sut fiveSidedDieFromSevenSidedDie];
    
    XCTAssertEqual(randomWasCalled, 5);
}

/*
 run through all the values from 0-34, make sure that 0-4 are each returned the same number of times
 */
- (void)testDistribution {
    // GAH, too late for this tonight.  Make 35 calls to fiveSidedDieFromSevenSidedDie
    // make sure that the calls to rand7 would sum to 0, 1, ... 34
    // make sure that the numbers 1-5 are each returned seven times.
    
    
}


@end
