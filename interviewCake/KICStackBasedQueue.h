//
//  KICStackBasedQueue.h
//  projectEuler
//
//  Created by Peter Kimball on 8/9/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KICStackBasedQueue : NSObject

- (void)enqueue:(id)object;
- (id)dequeue;
- (NSUInteger)count;

@end
