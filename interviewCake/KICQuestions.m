//
//  KICQuestions.m
//  projectEuler
//
//  Created by Peter Kimball on 8/8/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KICQuestions.h"

@implementation KICQuestions

/**
 * I recalled that a greedy algorithm was a good solution here, and I agree with that.
 *
 * Keep track of the best trade so far and the lowest value seen.
 * When the current stock price is lower than the lowest value seen, just update the lowest value, do not calculate a trade.
 * When the current trade is better than the best so far, note that of course!
 */
+ (float)bestAppleTrade:(NSArray *)stockPrices {

    // need placeholder values that will be immediately over-written
    float lowestPriceSoFar = FLT_MAX;
    float bestTradeSoFar = -FLT_MAX;
    
    for (id currentStockPriceObj in stockPrices) {
        assert([currentStockPriceObj isKindOfClass:[NSNumber class]]);
        
        float currentStockPrice = [(NSNumber *)currentStockPriceObj floatValue];
        float currentTrade = currentStockPrice - lowestPriceSoFar;
        
        if (currentStockPrice < lowestPriceSoFar) {
            lowestPriceSoFar = currentStockPrice;
        }
        
        if (currentTrade > bestTradeSoFar) {
            bestTradeSoFar = currentTrade;
        }
    }
    return bestTradeSoFar;
}

/**
 * Another one I've seen.  I recall my original solution was O(n), but not as efficient as theirs (time or space).
 * Arrays in ObjC annoy me
 */
+ (NSArray *)productOfAllExceptThisIndex:(NSArray *)input {
    if (input.count == 0) {
        return nil;
    }
    // storage = 3n
    NSMutableArray *answer = [NSMutableArray arrayWithCapacity:input.count];
    NSMutableArray *productOfAllToRightOfThisIndex = [input mutableCopy];
    NSMutableArray *productOfAllToLeftOfThisIndex = [NSMutableArray arrayWithCapacity:input.count];
    
    // setup
    productOfAllToLeftOfThisIndex[0] = @(1);
    productOfAllToRightOfThisIndex[input.count - 1] = @(1);
    
    for (NSInteger i = 1; i < input.count; i++) {
        assert([input[i] isKindOfClass:[NSNumber class]]);
        productOfAllToLeftOfThisIndex[i] = @([input[i - 1] integerValue] * [productOfAllToLeftOfThisIndex[i - 1] integerValue]);
    }
    
    // this is ugly
    for (NSInteger i = (input.count - 2); i >= 0; i--) {
        productOfAllToRightOfThisIndex[i] = @([input[i + 1] integerValue] * [productOfAllToRightOfThisIndex[i + 1] integerValue]);
    }
    
    for (NSInteger i = 0; i < input.count; i++) {
        answer[i] = @([productOfAllToLeftOfThisIndex[i] integerValue] * [productOfAllToRightOfThisIndex[i] integerValue]);
    }
    
    return answer;
}

/**
 * Amazon asked me this one a while back.
 * The cheap answer is to sort the array and take the three largest values (or possibly grab two negative values if available).  Complexity equal to that of the sorting algo (hopefully n log n)
 * The answer that requires attention to detail runs in linear time and constant space.
 */
+ (NSInteger)largestProductOfThreeIntegers:(NSArray *)input {
    assert(input.count >= 3);
    
    __block NSInteger small = NSIntegerMin;
    __block NSInteger medium = NSIntegerMin;
    __block NSInteger large = NSIntegerMin;
    
    __block NSInteger mostNegative = 0;
    __block NSInteger nextMostNegative = 0;
    
    void (^evaluateForProduct)(NSNumber *) = ^void(NSNumber *input) {
        NSInteger inputInteger = [input integerValue];
        
        if (inputInteger >= 0) {
            if (inputInteger > large) {
                small = medium;
                medium = large;
                large = inputInteger;
            } else if (inputInteger > medium) {
                small = medium;
                medium = inputInteger;
            } else if (inputInteger > small) {
                small = inputInteger;
            }
        } else {
            if (inputInteger < mostNegative) {
                nextMostNegative = mostNegative;
                mostNegative = inputInteger;
            } else if (inputInteger < nextMostNegative) {
                nextMostNegative = inputInteger;
            }
        }
    };
    
    for (int i = 0; i < input.count; i++) {
        assert([input[i] isKindOfClass:[NSNumber class]]);
        evaluateForProduct(input[i]);
    }
    
    NSInteger productUsingNegativeValues = mostNegative * nextMostNegative * large;
    NSInteger productUsingPositiveValues = small * medium * large;
    
    return MAX(productUsingNegativeValues, productUsingPositiveValues);
}



@end