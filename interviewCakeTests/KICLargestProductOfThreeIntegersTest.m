//
//  KICLargestProductOfThreeIntegersTest.m
//  projectEuler
//
//  Created by Peter Kimball on 8/8/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KICQuestions.h"

@interface KICLargestProductOfThreeIntegersTest : XCTestCase

@end

@implementation KICLargestProductOfThreeIntegersTest

- (void)testArrayOfPositiveIntegers {
    NSArray *input = @[ @(2), @(4), @(6) ];
    NSInteger expectedValue = 2 * 4 * 6;
    
    XCTAssertEqual([KICQuestions largestProductOfThreeIntegers:input], expectedValue);
}

- (void)testArrayWithTwoNegativeIntegers {
    NSArray *input = @[ @(2), @(4), @(6), @(-7), @(-5) ];
    NSInteger expectedValue = 6 * (-7) * (-5);
    
    XCTAssertEqual([KICQuestions largestProductOfThreeIntegers:input], expectedValue);
}

- (void)testArrayWithOneNegativeInteger {
    NSArray *input = @[ @(2), @(4), @(6), @(-10) ];
    NSInteger expectedValue = 2 * 4 * 6;
    
    XCTAssertEqual([KICQuestions largestProductOfThreeIntegers:input], expectedValue);
}

- (void)testThreePositiveConditionals {
    NSArray *input = @[ @(1), @(1), @(1) ];
    NSInteger expectedValue = 1 * 1 * 1;
    
    XCTAssertEqual([KICQuestions largestProductOfThreeIntegers:input], expectedValue);
}

- (void)testTwoNegativeConditionals {
    NSArray *input = @[ @(5), @(-5), @(-4) ];
    NSInteger expectedValue = 5 * (-5) * (-4);
    
    XCTAssertEqual([KICQuestions largestProductOfThreeIntegers:input], expectedValue);
}

@end
