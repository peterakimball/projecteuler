//
//  KPEIntegerFactorization.h
//  projectEuler
//
//  Created by Peter Kimball on 2/24/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPEIntegerFactorization : NSObject

+ (NSArray *)primeFactors:(NSUInteger)input;

@end
