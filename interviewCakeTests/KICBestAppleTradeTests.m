//
//  KICAppleStockQuestionTests.m
//  projectEuler
//
//  Created by Peter Kimball on 8/8/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KICQuestions.h"

@interface KICBestAppleTradeTests : XCTestCase

@end



@implementation KICBestAppleTradeTests

- (void)testSimpleRisingPrice {
    NSArray *stockPrices = @[ @(1.0), @(2.0), @(3.0), @(4.0), @(5.0) ];
    
    float expectedBestTrade = [stockPrices[stockPrices.count - 1] floatValue] - [stockPrices[0] floatValue];
    float calculatedBestTrade = [KICQuestions bestAppleTrade:stockPrices];
    
    XCTAssertEqual(expectedBestTrade, calculatedBestTrade);
}

/* According to the question, I must trade, even if the stock goes down all day */
- (void)testSimpleFallingPrice {
    NSArray *stockPrices = @[ @(5.0), @(4.0), @(3.0), @(2.0), @(1.0) ];
    
    float expectedBestTrade = -1.0;
    float calculatedBestTrade = [KICQuestions bestAppleTrade:stockPrices];
    
    XCTAssertEqual(expectedBestTrade, calculatedBestTrade);
}

- (void)testPriceGoesBelowOpen {
    NSArray *stockPrices = @[ @(5.0), @(6.0), @(7.0), @(8.0), @(4.0), @(5.0) ];
    
    float expectedBestTrade = [stockPrices[3] floatValue] - [stockPrices[0] floatValue];
    float calculatedBestTrade = [KICQuestions bestAppleTrade:stockPrices];
    
    XCTAssertEqual(expectedBestTrade, calculatedBestTrade);
}

- (void)testBestTradeIsAfterPriceGoesBelowOpen {
    NSArray *stockPrices = @[ @(5.0), @(6.0), @(7.0), @(8.0), @(4.0), @(5.0), @(6.0), @(7.0), @(8.0), @(9.0) ];
    
    float expectedBestTrade = [stockPrices[stockPrices.count - 1] floatValue] - [stockPrices[4] floatValue];
    float calculatedBestTrade = [KICQuestions bestAppleTrade:stockPrices];
    
    XCTAssertEqual(expectedBestTrade, calculatedBestTrade);
}

@end
