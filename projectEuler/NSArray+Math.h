//
//  NSArray+Math.h
//  projectEuler
//
//  Created by Peter Kimball on 2/14/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Math)

@property (nonatomic, readonly) NSString *integerString;

- (NSArray *)arrayByAddingDigitsFromArray:(NSArray *)otherArray;

@end
