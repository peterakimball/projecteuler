//
//  KPENamesScoresTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/17/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "KPENamesScores.h"

@interface KPENamesScoresTests : XCTestCase

@end

@implementation KPENamesScoresTests

static KPENamesScores *sut;

- (void)setUp {
    [super setUp];
    sut = [[KPENamesScores alloc] init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// passed on first try.
- (void)testSolve {
    XCTAssertEqualObjects([sut solve], @871198282, @"Project Euler agrees the solution is 871198282");
}

@end
