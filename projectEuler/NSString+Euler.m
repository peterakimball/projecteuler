//
//  NSString+Euler.m
//  projectEuler
//
//  Created by Peter Kimball on 2/21/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "NSString+Euler.h"

@implementation NSString (Euler)

- (NSUInteger)sumOfCharacters {
    // Plan - make all uppercase, get unichar value, deduct offset to A, score.
    // Needed to reference unichar values table.
    // I should really put some bounds-checking on this, as it's only intended for A-Z.
    NSString *uppercaseVersion = [self uppercaseString];
    
    unichar offset = 0x40;
    NSUInteger score = 0;
    
    for (int i = 0; i < uppercaseVersion.length; i++) {
        unichar c = [uppercaseVersion characterAtIndex:i];
        score += (c - offset);
    }
    
    return score;
}

@end
