//
//  KPEThousandDigitFibonacciNumber.m
//  projectEuler
//
//  Created by Peter Kimball on 2/14/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

//  Solution developed from earlier implementation in Swift


#import "KPEThousandDigitFibonacciNumber.h"
#import "NSArray+Math.h"

@implementation KPEThousandDigitFibonacciNumber

- (NSInteger)index {
    return 25;
}

- (NSString *)challenge {
    return @"The Fibonacci sequence is defined by the recurrence relation: Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.\n\
    Hence the first 12 terms will be:\n\
    \n\
    F1 = 1\n\
    F2 = 1\n\
    F3 = 2\n\
    F4 = 3\n\
    F5 = 5\n\
    F6 = 8\n\
    F7 = 13\n\
    F8 = 21\n\
    F9 = 34\n\
    F10 = 55\n\
    F11 = 89\n\
    F12 = 144\n\
    The 12th term, F12, is the first term to contain three digits.\n\
    What is the first term in the Fibonacci sequence to contain 1000 digits?";
}

- (NSString *)solution {
    return [NSString stringWithFormat:@"the first term in the Fibonacci sequence to contain 1000 digits %d", [[self solve] intValue]];
}

- (NSNumber *)solveForFibOfLength:(NSUInteger)digits {
    NSArray *fib = @[ @1 ];
    NSArray *fibMinusOne = @[ @1 ];
    unsigned int index = 2;
    
    while (fib.count < digits) {
        NSArray *tmp = fib;
        fib = [fib arrayByAddingDigitsFromArray:fibMinusOne];
        fibMinusOne = tmp;
        index++;
    }
    return [NSNumber numberWithUnsignedInt:index];
}

- (NSNumber *)solve {
    return [self solveForFibOfLength:1000];
}

@end
