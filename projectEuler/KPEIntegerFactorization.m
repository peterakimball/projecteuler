//
//  KPEIntegerFactorization.m
//  projectEuler
//
//  Created by Peter Kimball on 2/24/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KPEIntegerFactorization.h"

@implementation KPEIntegerFactorization

// looked up some stuff on interger factorization.  learned how a factor tree works.
// decided implementing a "real" factorization algorithm is not something I want to do at 11pm.


+ (NSArray *)treeAlgoFactorization:(NSUInteger)input {
    NSArray *result;
    // My intuition tells me I want a balanced tree.  Let's find the square root of the int and start there.
    NSUInteger candidate = (NSUInteger)sqrt(input);
    
    while (input % candidate != 0) {
        candidate--;
    }

    if ((candidate == 1) || (input / candidate == 1)) {
        result = @[ [NSNumber numberWithInteger:input] ];
    } else {
        NSArray *leftArray = [KPEIntegerFactorization treeAlgoFactorization:candidate];
        NSArray *rightArray = [KPEIntegerFactorization treeAlgoFactorization:input / candidate];
        result = [leftArray arrayByAddingObjectsFromArray:rightArray];
    }
    
    return result;
}

+ (NSArray *)primeFactors:(NSUInteger)input {
    
    return [KPEIntegerFactorization treeAlgoFactorization:input];
}

@end
