//
//  KPEThousandDigitFibonacciNumber.h
//  projectEuler
//
//  Created by Peter Kimball on 2/14/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPEPresenter.h"

@interface KPEThousandDigitFibonacciNumber : NSObject <KPEProblem>

- (NSNumber *)solveForFibOfLength:(NSUInteger)digits;

@end
