//
//  KPEIntegerFactorizationTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/24/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "KPEIntegerFactorization.h"

@interface KPEIntegerFactorizationTests : XCTestCase

@end

@implementation KPEIntegerFactorizationTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFactorsOfOne {
    NSArray *result = [KPEIntegerFactorization primeFactors:1];
    XCTAssertEqualObjects(result, @[ @1 ], "Prime factor of 1 is 1, sorta");
}

- (void)testFactorsOfTwo {
    NSArray *result = [KPEIntegerFactorization primeFactors:2];
    XCTAssertEqualObjects(result, @[ @2 ], "Prime factor of 2 is 2");
}

- (void)testFactorsOfThree {
    NSArray *result = [KPEIntegerFactorization primeFactors:3];
    XCTAssertEqualObjects(result, @[ @3 ], "Prime factor of 3 is 3");
}

- (void)testFactorsOfFour {
    NSArray *result = [KPEIntegerFactorization primeFactors:4];
    NSArray *answer = @[ @2, @2 ];
    XCTAssertEqualObjects(result, answer, "Prime factors of 4 are 2 and 2");
}

- (void)testFactorsOfFourteen {
    NSArray *result = [KPEIntegerFactorization primeFactors:14];
    NSArray *answer = @[ @2, @7 ];
    XCTAssertEqualObjects(result, answer, "Prime factors of 14 are 2 and 7");
}

- (void)testFactorsOfFifteen {
    NSArray *result = [KPEIntegerFactorization primeFactors:15];
    NSArray *answer = @[ @3, @5 ];
    XCTAssertEqualObjects(result, answer, "Prime factors of 15 are 3 and 5");
}

- (void)testFactorsOf644 {
    NSArray *result = [[KPEIntegerFactorization primeFactors:644] sortedArrayUsingSelector:@selector(compare:)];
    NSArray *answer = @[ @2, @2, @7, @23 ];
    XCTAssertEqualObjects(result, answer, "Prime factors of 644 are 2, 2, 7, and 23");
}

- (void)testFactorsOf645 {
    NSArray *result = [[KPEIntegerFactorization primeFactors:645] sortedArrayUsingSelector:@selector(compare:)];
    NSArray *answer = @[ @3, @5, @43 ];
    XCTAssertEqualObjects(result, answer, "Prime factors of 645 are 3, 5, and 43");
}

- (void)testFactorsOf646 {
    NSArray *result = [[KPEIntegerFactorization primeFactors:646] sortedArrayUsingSelector:@selector(compare:)];
    NSArray *answer = @[ @2, @17, @19 ];
    XCTAssertEqualObjects(result, answer, "Prime factors of 645 are 2, 17, and 19");
}

@end
