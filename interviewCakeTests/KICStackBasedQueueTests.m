//
//  KICStackBasedQueueTests.m
//  projectEuler
//
//  Created by Peter Kimball on 8/9/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KICStackBasedQueue.h"

@interface KICStackBasedQueueTests : XCTestCase

@property (nonatomic) KICStackBasedQueue *sut;

@end

@implementation KICStackBasedQueueTests

- (void)setUp {
    self.sut = [KICStackBasedQueue new];
    [super setUp];
}

- (void)testInitialCountZero {
    XCTAssertEqual(self.sut.count, 0);
}

- (void)testDequeueOnEmptyQueueIsNil {
    XCTAssertNil([self.sut dequeue]);
}

- (void)testCountAfterEnqueueOnly {
    NSUInteger expectedCount = 3;
    
    for (NSUInteger i = 0; i < expectedCount; i++) {
        [self.sut enqueue:@(i)];
    }
    
    XCTAssertEqual(self.sut.count, expectedCount);
}

- (void)testCountAfterDequque {
    NSUInteger expectedCount = 3;
    
    for (NSUInteger i = 0; i < expectedCount; i++) {
        [self.sut enqueue:@(i)];
    }
    
    id obj = [self.sut dequeue];
    [self.sut enqueue:obj];
    
    XCTAssertEqual(self.sut.count, expectedCount);
}

- (void)testIsFIFONotMixedOperations {
    NSUInteger expectedCount = 3;
    
    // prepare
    for (NSUInteger i = 0; i < expectedCount; i++) {
        [self.sut enqueue:@(i)];
    }

    // check
    for (NSUInteger i = 0; i < expectedCount; i++) {
        XCTAssertEqualObjects([self.sut dequeue], @(i));
    }
}

- (void)testIsFIFOMixedOperations {
    NSUInteger expectedCount = 3;
    
    // prepare
    for (NSUInteger i = 0; i < expectedCount; i++) {
        [self.sut enqueue:@(i)];
    }
    for (NSUInteger i = 0; i < (expectedCount / 2); i++) {
        [self.sut dequeue];
    }
    for (NSUInteger i = 0; i < expectedCount / 2; i++) {
        [self.sut enqueue:@(i)];
    }
    
    // check
    for (NSUInteger i = expectedCount / 2; i < expectedCount; i++) {
        XCTAssertEqualObjects([self.sut dequeue], @(i));
    }
    for (NSUInteger i = 0; i < expectedCount / 2; i++) {
        XCTAssertEqualObjects([self.sut dequeue], @(i));
    }
}

@end