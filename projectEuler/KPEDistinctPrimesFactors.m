//
//  KPEDistinctPrimesFactors.m
//  projectEuler
//
//  Created by Peter Kimball on 2/25/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KPEDistinctPrimesFactors.h"
#import "KPEIntegerFactorization.h"

@implementation KPEDistinctPrimesFactors {
    NSMutableArray *_integerStack;
}

- (id)init {
    self = [super init];
    if (self) {
        _integerStack = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSInteger)index {
    return 47;
}

- (NSString *)challenge {
    return @"The first two consecutive numbers to have two distinct prime factors are:\n\
14 = 2 × 7\n\
15 = 3 × 5\n\
The first three consecutive numbers to have three distinct prime factors are:\n\
644 = 2² × 7 × 23\n\
645 = 3 × 5 × 43\n\
646 = 2 × 17 × 19.\n\
Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?";
}

- (NSString *)solution {
    return [NSString stringWithFormat:@"The first four consecutive integers to have four distinct prime factors. The first of these numbers is %@", [self solve]];
}

- (NSNumber *)solve {
    
    NSMutableSet *factorSet = [[NSMutableSet alloc] init];
    
    for (NSUInteger i = 646; i < NSUIntegerMax; i++) {
        [factorSet removeAllObjects];
        [factorSet addObjectsFromArray:[KPEIntegerFactorization primeFactors:i]];
        if (factorSet.count == 4) {
            [_integerStack addObject:[NSNumber numberWithUnsignedInteger:i]];
        } else {
            [_integerStack removeAllObjects];
        }
        if (_integerStack.count == 4) {
            break;
        }
    }
    
    return _integerStack[0];
}

@end
