//
//  NSString+EulerTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/21/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Euler.h"

@interface NSString_EulerTests : XCTestCase

@end

@implementation NSString_EulerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testScoreA {
    XCTAssertEqual([@"a" sumOfCharacters], 1, @"sum of characters for the string \"a\" is 1");
}

- (void)testScoreEmpty {
    XCTAssertEqual([@"" sumOfCharacters], 0, @"alphabetical value for empty string is 0");
}

- (void)testScoreMixedCase {
    XCTAssertEqual([@"aZb" sumOfCharacters], 29, @"alphabetical value for the string \"aZb\" is 1 + 26 + 2 = 29");
}

@end
