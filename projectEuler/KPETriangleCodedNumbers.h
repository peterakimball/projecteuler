//
//  KPETriangleCodedNumbers.h
//  projectEuler
//
//  Created by Peter Kimball on 2/22/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPEPresenter.h"

@interface KPETriangleCodedNumbers : NSObject <KPEProblem>

@end
