//
//  KICDice.m
//  projectEuler
//
//  Created by Peter Kimball on 8/10/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KICDice.h"

@implementation KICDice {
    NSUInteger (^_rand)(u_int32_t);
}

- (instancetype)initWithRandomGenerator:(NSUInteger (^)(u_int32_t))randomGenerator {
    self = [super init];
    if (self) {
        _rand = randomGenerator;
    }
    return self;
}

- (instancetype)init {
    return [self initWithRandomGenerator:^NSUInteger (u_int32_t randMax) {
        return arc4random_uniform(randMax);
    }];
}

/**
 *
 You have a function rand7() that generates a random integer from 1 to 7. Use it to write a function rand5() that generates a random integer from 1 to 5.
 
 sucker's bet is to simply use rand7 % 5, but that introduces a modulo bias.  let's avoid that.
 
 we want something that's divisible by both 7 and 5: 35 would work.
 the source int has to be evenly distributed between 0 and 34 (inclusive)
 this is possible if we create the sum of five calls to rand7
 return this source int % 5, adding 1 because die don't start at 0.
 
 */

- (NSUInteger)fiveSidedDieFromSevenSidedDie {
    
    NSUInteger source = 0;
    u_int32_t randMax = 7;
    NSUInteger outMax = 5;
    
    for (int i = 0; i < outMax; i++) {
        source += _rand(randMax);
    }
    
    return (source % outMax) + 1;
}

@end
