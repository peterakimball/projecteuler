//
//  KICDice.h
//  projectEuler
//
//  Created by Peter Kimball on 8/10/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KICDice : NSObject

- (instancetype)initWithRandomGenerator:(NSUInteger (^)(u_int32_t))randomGenerator;
- (NSUInteger)fiveSidedDieFromSevenSidedDie;

@end
