//
//  KPENamesScores.m
//  projectEuler
//
//  Created by Peter Kimball on 2/17/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KPENamesScores.h"
#import "NSString+Euler.h"

@implementation KPENamesScores

- (NSInteger)index {
    return 22;
}

- (NSString *)challenge {
    return @"Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.\n\nFor example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.\n\nWhat is the total of all the name scores in the file?";
}

- (NSString *)solution {
    return [NSString stringWithFormat:@"The total of all the name scores for the file names.txt is %@", [self solve]];
}

- (NSNumber *)solve {
    // I'm loading this file from my local disk - you can load it from the web, if you would like
    // Just don't do it too often!
    // NSURL *namesFileURL = [NSURL URLWithString:@"https://projecteuler.net/project/resources/p022_names.txt"];
    NSURL *namesFileURL = [NSURL URLWithString:@"file:////Users/kimball/Downloads/p022_names.txt"];
    NSString *namesFile = [NSString stringWithContentsOfURL:namesFileURL encoding:NSUTF8StringEncoding error:nil];

    // remove the beginning and ending "
    NSString *trimmed = [namesFile stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    // separate by ","
    NSArray *names = [trimmed componentsSeparatedByString:@"\",\""];
    
    // Learned this from the NSArray docs.  Cool.
    NSArray *sortedNames = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    NSUInteger sumOfNameScores = 0;
    
    // Project Euler typically starts from index 1
    for (int i = 0; i < sortedNames.count; i++) {
        sumOfNameScores += ((i + 1) * [sortedNames[i] sumOfCharacters]);
    }
    
    return [NSNumber numberWithUnsignedInteger:sumOfNameScores];
}

//- (NSUInteger)alphabeticalValue:(NSString *)input {
//    
//    // Plan - make all uppercase, get unichar value, deduct offset to A, score.
//    // Needed to reference unichar values table.
//    // I should really put some bounds-checking on this, as it's only intended for A-Z.
//    NSString *uppercaseVersion = [input uppercaseString];
//    
//    unichar offset = 0x40;
//    NSUInteger score = 0;
//    
//    for (int i = 0; i < uppercaseVersion.length; i++) {
//        unichar c = [uppercaseVersion characterAtIndex:i];
//        score += (c - offset);
//    }
//    
//    return score;
//}

@end
