//
//  KICQuestions.h
//  projectEuler
//
//  Created by Peter Kimball on 8/8/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Questions from the Interview Cake website - I've done this before, but it's been several months.
 */

@interface KICQuestions : NSObject

+ (float)bestAppleTrade:(NSArray *)stockPrices;
+ (NSArray *)productOfAllExceptThisIndex:(NSArray *)input;
+ (NSInteger)largestProductOfThreeIntegers:(NSArray *)input;

@end
