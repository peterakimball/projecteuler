//
//  KPEDoubleBasePalindromes.h
//  projectEuler
//
//  Created by Peter Kimball on 2/7/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPEPresenter.h"

@interface KPEDoubleBasePalindromes : NSObject <KPEProblem>

- (BOOL)isBase2Palindrome:(unsigned)candidate;
- (BOOL)isBase10Palindrome:(unsigned)candidate;

@end
