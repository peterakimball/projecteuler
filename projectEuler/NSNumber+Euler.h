//
//  NSNumber+Euler.h
//  projectEuler
//
//  Created by Peter Kimball on 2/21/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Euler)

- (BOOL)isTriangleNumber;

@end
