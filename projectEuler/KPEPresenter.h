//
//  KPEPresenter.h
//  projectEuler
//
//  Created by Peter Kimball on 2/7/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KPEProblem

@property (readonly) NSInteger index;
@property (readonly) NSString *challenge;
@property (readonly) NSString *solution;

- (NSNumber *)solve;

@end

@interface KPEPresenter : NSObject

@end
