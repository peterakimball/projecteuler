//
//  KPETriangleCodedNumbersTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/22/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "KPETriangleCodedNumbers.h"

@interface KPETriangleCodedNumbersTests : XCTestCase

@end

@implementation KPETriangleCodedNumbersTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSolve {
    KPETriangleCodedNumbers *sut = [KPETriangleCodedNumbers new];
    XCTAssertEqual([[sut solve] unsignedIntValue], 162, @"Project Euler confirms that the solution is 162");
}


@end
