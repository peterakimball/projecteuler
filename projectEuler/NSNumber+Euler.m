//
//  NSNumber+Euler.m
//  projectEuler
//
//  Created by Peter Kimball on 2/21/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "NSNumber+Euler.h"

static NSMutableArray *_triangleNumbers;

@implementation NSNumber (Euler)

- (void)calculateTriangleNumbers {
    @synchronized (_triangleNumbers) {
        while ([self compare:[_triangleNumbers lastObject]] == NSOrderedDescending) {
            NSUInteger index = [_triangleNumbers count] + 1;
            NSUInteger triangleAtIndex = (0.5 * (float)index) * (index + 1);
            [_triangleNumbers addObject:[NSNumber numberWithInteger:triangleAtIndex]];
        }
    }
}

- (void)initTriangleNumbers {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _triangleNumbers = [NSMutableArray new];
        [_triangleNumbers addObject:@1];
    });
}

- (NSUInteger)binarySearch {
    NSUInteger searchResult = [_triangleNumbers indexOfObject:self inSortedRange:NSMakeRange(0, _triangleNumbers.count) options:0 usingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];
    return searchResult;
}

- (NSUInteger)linearSearch {
    NSUInteger searchResult = NSNotFound;
    for (int i = 0; i < _triangleNumbers.count; i++) {
        NSComparisonResult comparisonResult = [self compare:_triangleNumbers[i]];
        if (comparisonResult == NSOrderedSame) {
            searchResult = i;
            break;
        } else if (comparisonResult == NSOrderedAscending) {
            break;
        }
    }
    return searchResult;
}

- (BOOL)isTriangleNumber {
    [self initTriangleNumbers];
    
    if ([self compare:[_triangleNumbers lastObject]] == NSOrderedDescending) {
        [self calculateTriangleNumbers];
    }
    
    NSUInteger searchResult;
    
    if (_triangleNumbers.count < 100) {
        searchResult = [self linearSearch];
    } else {
        searchResult = [self binarySearch];
    }
    
    return searchResult != NSNotFound;
}

@end
