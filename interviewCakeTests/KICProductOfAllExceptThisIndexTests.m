//
//  KICProductOfAllExceptThisIndexTests.m
//  projectEuler
//
//  Created by Peter Kimball on 8/8/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KICQuestions.h"

@interface KICProductOfAllExceptThisIndexTests : XCTestCase

@end

@implementation KICProductOfAllExceptThisIndexTests

- (void)testWithExampleArray {
    NSArray *input = @[ @(1), @(7), @(3), @(4) ];
    NSArray *expectedAnswer = @[ @(84), @(12), @(28), @(21) ];

    XCTAssertEqualObjects([KICQuestions productOfAllExceptThisIndex:input], expectedAnswer);
}


- (void)testWithZeros {
    NSArray *input = @[ @(1), @(7), @(0), @(4) ];
    NSArray *expectedAnswer = @[ @(0), @(0), @(28), @(0) ];
    
    XCTAssertEqualObjects([KICQuestions productOfAllExceptThisIndex:input], expectedAnswer);
}

@end
