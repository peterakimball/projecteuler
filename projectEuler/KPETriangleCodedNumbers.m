//
//  KPETriangleCodedNumbers.m
//  projectEuler
//
//  Created by Peter Kimball on 2/22/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KPETriangleCodedNumbers.h"
#import "NSString+Euler.h"
#import "NSNumber+Euler.h"

@implementation KPETriangleCodedNumbers

- (NSInteger)index {
    return 42;
}

- (NSString *)challenge {
    return @"The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are: 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ... \n\
By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.\n\
Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?";
}

- (NSString *)solution {
    return [NSString stringWithFormat:@"There are %@ triangle words in the given file", [self solve]];
}

- (NSNumber *)solve {
    // I'm loading this file from my local disk - you can load it from the web, if you would like
    // Just don't do it too often!
    // NSURL *namesFileURL = [NSURL URLWithString:@"https://projecteuler.net/project/resources/p042_words.txt"];
    NSURL *wordsFileURL = [NSURL URLWithString:@"file:////Users/kimball/Downloads/p042_words.txt"];
    NSString *wordsFile = [NSString stringWithContentsOfURL:wordsFileURL encoding:NSUTF8StringEncoding error:nil];
    
    // remove the beginning and ending "
    NSString *trimmed = [wordsFile stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    // separate by ","
    NSArray *words = [trimmed componentsSeparatedByString:@"\",\""];
    
    NSUInteger triangleWords = 0;
    
    for (int i = 0; i < words.count; i++) {
        NSUInteger sum = [(NSString *)words[i] sumOfCharacters];
        NSNumber *value = [NSNumber numberWithUnsignedInteger:sum];
        if ([value isTriangleNumber]) {
            triangleWords++;
        }
    }
    
    return [NSNumber numberWithUnsignedInteger:triangleWords];
}

@end
