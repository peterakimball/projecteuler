//
//  NSArray+Math.m
//  projectEuler
//
//  Created by Peter Kimball on 2/14/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "NSArray+Math.h"

@implementation NSArray (Math)

- (NSString *)integerString {
    NSMutableString *result = [[NSMutableString alloc] init];
    for (int i = (int)self.count - 1; i >= 0; i--) {
        [result appendString:[self[i] stringValue]];
    }
    return result;
}

- (NSArray *)arrayByAddingDigitsFromArray:(NSArray *)otherArray {
    
    NSArray *shorterArray;
    NSArray *longerArray;
    
    if (self.count > otherArray.count) {
        longerArray = self;
        shorterArray = otherArray;
    } else {
        longerArray = otherArray;
        shorterArray = self;
    }
    
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:longerArray.count];
    NSUInteger carry = 0;
    int i = 0;
    
    while (i < longerArray.count || carry > 0) {
        NSUInteger sum;
        if (i == longerArray.count) {
            sum = carry;
        } else if (i >= shorterArray.count) {
            sum = [longerArray[i] unsignedIntegerValue] + 0 + carry;
        } else {
            sum = [longerArray[i] unsignedIntegerValue] + [shorterArray[i] unsignedIntegerValue] + carry;
        }
        carry = sum / 10;
        [result addObject:[NSNumber numberWithUnsignedInteger:sum % 10]];
        i++;
    }
    return result;
}

@end
