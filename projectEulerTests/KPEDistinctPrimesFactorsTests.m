//
//  KPEDistinctPrimesFactorsTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/25/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "KPEDistinctPrimesFactors.h"

@interface KPEDistinctPrimesFactorsTests : XCTestCase

@end

@implementation KPEDistinctPrimesFactorsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSolution {
    // this was really pretty boring.  Either it works or it don't.
    // Yes, that is intentionally bad English, and yes, this doesn't even
    // qualify as a unit test.
    KPEDistinctPrimesFactors *sut = [[KPEDistinctPrimesFactors alloc] init];
    XCTAssertEqualObjects([sut solve], @134043, @"Project Euler agrees that the answer is 134043");
}

@end
