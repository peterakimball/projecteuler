//
//  KPEThousandDigitFibonacciNumberTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/14/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "KPEThousandDigitFibonacciNumber.h"

@interface KPEThousandDigitFibonacciNumberTests : XCTestCase

@end

@implementation KPEThousandDigitFibonacciNumberTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testForFibOfLengthTwo {
    KPEThousandDigitFibonacciNumber *sut = [[KPEThousandDigitFibonacciNumber alloc] init];
    XCTAssertEqual([[sut solveForFibOfLength:2] intValue], 7, @"The first two-digit Fibonacci number is F7");
}

- (void)testForFibOfLengthThree {
    KPEThousandDigitFibonacciNumber *sut = [[KPEThousandDigitFibonacciNumber alloc] init];
    XCTAssertEqual([[sut solveForFibOfLength:3] intValue], 12, @"The first three-digit Fibonacci number is F12");
}

- (void)testSolve {
    KPEThousandDigitFibonacciNumber *sut = [[KPEThousandDigitFibonacciNumber alloc] init];
    XCTAssertEqual([[sut solve] intValue], 4782, @"Project Euler agrees that the solution is 4782");
}


@end
