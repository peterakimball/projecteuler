//
//  NSNumber+EulerTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/21/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "NSNumber+Euler.h"

@interface NSNumber_EulerTests : XCTestCase

@end

@implementation NSNumber_EulerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testTriangleNumbersExample {
    NSArray *exampleTriangleNumbers = @[ @1, @3, @6, @10, @15, @21, @28, @36, @45, @55 ];
    NSUInteger matchedNumbers = 0;
    
    for (int i = 0; i <= [[exampleTriangleNumbers lastObject] intValue]; i++) {
        NSNumber *current = [NSNumber numberWithInt:i];
        if ([current isTriangleNumber]) {
            matchedNumbers++;
        }
    }
    XCTAssertEqual(matchedNumbers, exampleTriangleNumbers.count, @"Expected %u matching numbers", (unsigned int)exampleTriangleNumbers.count);
}

@end
