//
//  KPEDoubleBasePalindromes.m
//  projectEuler
//
//  Created by Peter Kimball on 2/7/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import "KPEDoubleBasePalindromes.h"

@implementation KPEDoubleBasePalindromes

- (NSInteger)index {
    return 36;
}

- (NSString *)challenge {
    return @"The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.  Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.  (Please note that the palindromic number, in either base, may not include leading zeros.)";
}

- (NSString *)solution {
    return [NSString stringWithFormat:@"the sum of all the base 10 and base 2 palindromic numbers less than one million is %d", [[self solve] intValue]];
}

- (NSNumber *)solve {
	return [self solveForIntegersLessThan:1000000];
}

- (NSNumber *)solveForIntegersLessThan:(unsigned)max {
	unsigned sum = 0;
	
	for (int i = 1; i < max; i++) {
		if ([self isBase10Palindrome:i] && [self isBase2Palindrome:i]) {
			sum += i;
		}
	}
	return [NSNumber numberWithUnsignedInt:sum];
}

// no reference materials consulted for this method
- (BOOL)isBase2Palindrome:(unsigned)candidate {
    unsigned width = 8 * sizeof(unsigned);
    unsigned checkLeft = 1 << (width - 1);
    unsigned checkRight = 1;
    
    // shortcut around 0 and 1
    if (candidate < 1) {
        return NO;
    }
    
    // find leftmost bit
    while((candidate & checkLeft) == 0) {
        checkLeft = checkLeft >> 1;
    }
    
    // stop moving check-bits when they meet
    while(checkLeft > checkRight) {
        // check the bits match
        BOOL leftBitIsSet = (checkLeft & candidate) > 0;
        BOOL rightBitIsSet = (checkRight & candidate) > 0;
        
        if(leftBitIsSet != rightBitIsSet) {
            return NO;
        }
        // move the check bits closer together
        checkRight = checkRight << 1;
        checkLeft = checkLeft >> 1;
    }
    return YES;
}

- (unsigned)countBase10Digits:(unsigned)input {
	unsigned digits = 0;
	
	while (input != 0) {
		input /= 10;
		digits++;
	}
	
	return digits;
}


// referenced http://en.wikipedia.org/wiki/Palindromic_number to learn if single-digit numbers are palindromes - they are.
// tactic - check most & least sig digits, then strip those, continue until digits don't match, or reach single/no digit.
// e.g. 98789 -
// 98789
// 878
// 7
- (BOOL)isBase10Palindrome:(unsigned)candidate {
	
	unsigned numberOfDigits = [self countBase10Digits:candidate];
	
	unsigned mostSigDigitDivisor = (unsigned)powf(10, numberOfDigits - 1);
	unsigned leastSigDigitDivisor = 10;
	
	unsigned mSD = candidate / mostSigDigitDivisor;
	unsigned lSD = candidate % leastSigDigitDivisor;
	
	while (mSD == lSD && mostSigDigitDivisor > 0) {
		candidate -= mSD * mostSigDigitDivisor;
		candidate /= leastSigDigitDivisor;
		mostSigDigitDivisor /= 100;
		
		if (mostSigDigitDivisor > 0) {
			mSD = candidate / mostSigDigitDivisor;
			lSD = candidate % leastSigDigitDivisor;
		}
	}
	
	// return YES iff we have compared all digits
	return mostSigDigitDivisor == 0;
}


@end
