//
//  KPEDoubleBasePalindromesTests.m
//  projectEuler
//
//  Created by Peter Kimball on 2/7/15.
//  Copyright (c) 2015 Peter Kimball. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "KPEDoubleBasePalindromes.h"

@interface KPEDoubleBasePalindromesTests : XCTestCase

@end

@implementation KPEDoubleBasePalindromesTests

static KPEDoubleBasePalindromes *sut;

- (void)setUp {
    [super setUp];
	sut = [[KPEDoubleBasePalindromes alloc] init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testZeroIsNotABinaryPalindrome {
    unsigned notBinaryPalindrome = 0;
    XCTAssertFalse([sut isBase2Palindrome:notBinaryPalindrome]);
}

- (void)testOneIsABinaryPalindrome {
    unsigned binaryPalindrome = 1;
    XCTAssertTrue([sut isBase2Palindrome:binaryPalindrome]);
}

- (void)testBinaryPalindromeWithOnes {
    unsigned binaryPalindrome = 0b11;
    XCTAssertTrue([sut isBase2Palindrome:binaryPalindrome]);
}

- (void)testBinaryPalindromeWithOnesAndZeros {
    unsigned binaryPalindrome = 0b1001;
    XCTAssertTrue([sut isBase2Palindrome:binaryPalindrome]);
}

- (void)testNotBinaryPalindromeWithOnesAndZeros {
    unsigned notBinaryPalindrome = 0b1101;
    XCTAssertFalse([sut isBase2Palindrome:notBinaryPalindrome]);
}

- (void)testBinaryPalindromeOfOddLength {
    unsigned binaryPalindrome = 0b10101;
    XCTAssertTrue([sut isBase2Palindrome:binaryPalindrome]);
}

- (void)testNotBinaryPalindromeOfOddLength {
    unsigned notBinaryPalindrome = 0b10111;
    XCTAssertFalse([sut isBase2Palindrome:notBinaryPalindrome]);
}

- (void)testBase10Palindrome {
	unsigned input = 909;
	XCTAssertTrue([sut isBase10Palindrome:input], @"909 is a palindrome");
}

- (void)testOneIsABase10Palindrome {
	unsigned input = 1;
	XCTAssertTrue([sut isBase10Palindrome:input], @"1 is a palindrome");
}

- (void)testBase10PalindromeOfEvenLength {
	unsigned input = 9009;
	XCTAssertTrue([sut isBase10Palindrome:input], @"9009 is a palindrome");
}

- (void)testBase10NotAPalindrome {
	unsigned shouldNotBeABase10Palindrome = 102101;
	XCTAssertFalse([sut isBase10Palindrome:shouldNotBeABase10Palindrome],@"102101 is not a base10 palindrome");
}

- (void)testExample {
	unsigned doubleBasePalindrome = 585;
	BOOL isBase10Palindrome = [sut isBase10Palindrome:doubleBasePalindrome];
	BOOL isBase2Palindrome = [sut isBase2Palindrome:doubleBasePalindrome];
	XCTAssertTrue(isBase10Palindrome && isBase2Palindrome, @"585 is a double-base palindrome");
}

- (void)testBase10PassedOnEarlyExit {
	unsigned shouldNotBeABase10Palindrome = 10011;
	XCTAssertFalse([sut isBase10Palindrome:shouldNotBeABase10Palindrome],@"10011 is not a base10 palindrome");
}

- (void)testSolve {
	XCTAssertEqual([[sut solve] intValue], 872187, @"Project Euler agrees that the solution is 872187");
}

@end
